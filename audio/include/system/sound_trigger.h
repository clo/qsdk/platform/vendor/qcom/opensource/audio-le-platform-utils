/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.
 * Not a contribution.
 *
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_SOUND_TRIGGER_H
#define ANDROID_SOUND_TRIGGER_H

#include <stdbool.h>
#ifdef FEATURE_IPQ_OPENWRT

#include <audio_utils/bitops.h>

#define AUDIO_IO_HANDLE_NONE 0
#define AUDIO_DEVICE_NONE 0
#define AUDIO_CHANNEL_COUNT_MAX 30
#define AUDIO_CHANNEL_REPRESENTATION_LOG2 2

typedef uint32_t audio_channel_mask_t;
typedef int audio_io_handle_t;
typedef uint32_t audio_devices_t;

typedef enum {
    /* All of these are in native byte order */
    AUDIO_FORMAT_PCM_SUB_16_BIT          = 0x1, /* DO NOT CHANGE - PCM signed 16 bits */
    AUDIO_FORMAT_PCM_SUB_8_BIT           = 0x2, /* DO NOT CHANGE - PCM unsigned 8 bits */
    AUDIO_FORMAT_PCM_SUB_32_BIT          = 0x3, /* PCM signed .31 fixed point */
    AUDIO_FORMAT_PCM_SUB_8_24_BIT        = 0x4, /* PCM signed 8.23 fixed point */
    AUDIO_FORMAT_PCM_SUB_FLOAT           = 0x5, /* PCM single-precision floating point */
    AUDIO_FORMAT_PCM_SUB_24_BIT_PACKED   = 0x6, /* PCM signed .23 fixed point packed in 3 bytes */
} audio_format_pcm_sub_fmt_t;

typedef enum {
    AUDIO_FORMAT_MP3_SUB_NONE            = 0x0,
} audio_format_mp3_sub_fmt_t;

typedef enum {
    AUDIO_FORMAT_AMR_SUB_NONE            = 0x0,
} audio_format_amr_sub_fmt_t;

typedef enum {
    AUDIO_FORMAT_AAC_SUB_MAIN            = 0x1,
    AUDIO_FORMAT_AAC_SUB_LC              = 0x2,
    AUDIO_FORMAT_AAC_SUB_SSR             = 0x4,
    AUDIO_FORMAT_AAC_SUB_LTP             = 0x8,
    AUDIO_FORMAT_AAC_SUB_HE_V1           = 0x10,
    AUDIO_FORMAT_AAC_SUB_SCALABLE        = 0x20,
    AUDIO_FORMAT_AAC_SUB_ERLC            = 0x40,
    AUDIO_FORMAT_AAC_SUB_LD              = 0x80,
    AUDIO_FORMAT_AAC_SUB_HE_V2           = 0x100,
    AUDIO_FORMAT_AAC_SUB_ELD             = 0x200,
} audio_format_aac_sub_fmt_t;

typedef enum {
    AUDIO_FORMAT_VORBIS_SUB_NONE         = 0x0,
} audio_format_vorbis_sub_fmt_t;

typedef enum {
    AUDIO_FORMAT_INVALID             = 0xFFFFFFFFUL,
    AUDIO_FORMAT_DEFAULT             = 0,
    AUDIO_FORMAT_PCM                 = 0x00000000UL, /* DO NOT CHANGE */
    AUDIO_FORMAT_MP3                 = 0x01000000UL,
    AUDIO_FORMAT_AMR_NB              = 0x02000000UL,
    AUDIO_FORMAT_AMR_WB              = 0x03000000UL,
    AUDIO_FORMAT_AAC                 = 0x04000000UL,
    AUDIO_FORMAT_HE_AAC_V1           = 0x05000000UL, /* Deprecated, Use AUDIO_FORMAT_AAC_HE_V1*/
    AUDIO_FORMAT_HE_AAC_V2           = 0x06000000UL, /* Deprecated, Use AUDIO_FORMAT_AAC_HE_V2*/
    AUDIO_FORMAT_VORBIS              = 0x07000000UL,
    AUDIO_FORMAT_OPUS                = 0x08000000UL,
    AUDIO_FORMAT_AC3                 = 0x09000000UL,
    AUDIO_FORMAT_E_AC3               = 0x0A000000UL,
    AUDIO_FORMAT_DTS                 = 0x0B000000UL,
    AUDIO_FORMAT_DTS_HD              = 0x0C000000UL,
    // IEC61937 is encoded audio wrapped in 16-bit PCM.
    AUDIO_FORMAT_IEC61937            = 0x0D000000UL,
    AUDIO_FORMAT_DOLBY_TRUEHD        = 0x0E000000UL,
    AUDIO_FORMAT_EVRC                = 0x10000000UL,
    AUDIO_FORMAT_EVRCB               = 0x11000000UL,
    AUDIO_FORMAT_EVRCWB              = 0x12000000UL,
    AUDIO_FORMAT_EVRCNW              = 0x13000000UL,
    AUDIO_FORMAT_AAC_ADIF            = 0x14000000UL,
    AUDIO_FORMAT_WMA                 = 0x15000000UL,
    AUDIO_FORMAT_WMA_PRO             = 0x16000000UL,
    AUDIO_FORMAT_AMR_WB_PLUS         = 0x17000000UL,
    AUDIO_FORMAT_MP2                 = 0x18000000UL,
    AUDIO_FORMAT_QCELP               = 0x19000000UL,
    AUDIO_FORMAT_DSD                 = 0x1A000000UL,
    AUDIO_FORMAT_FLAC                = 0x1B000000UL,
    AUDIO_FORMAT_ALAC                = 0x1C000000UL,
    AUDIO_FORMAT_APE                 = 0x1D000000UL,
    AUDIO_FORMAT_AAC_ADTS            = 0x1E000000UL,
    AUDIO_FORMAT_SBC                 = 0x1F000000UL,
    AUDIO_FORMAT_APTX                = 0x20000000UL,
    AUDIO_FORMAT_APTX_HD             = 0x21000000UL,
    AUDIO_FORMAT_APTX_ADAPTIVE       = 0x25000000UL,
    AUDIO_FORMAT_AAC_LATM            = 0x80000000UL,

    AUDIO_FORMAT_MAIN_MASK           = 0xFF000000UL,
    AUDIO_FORMAT_SUB_MASK            = 0x00FFFFFFUL,

    /* Aliases */
    /* note != AudioFormat.ENCODING_PCM_16BIT */
    AUDIO_FORMAT_PCM_16_BIT          = (AUDIO_FORMAT_PCM |
                                        AUDIO_FORMAT_PCM_SUB_16_BIT),
    /* note != AudioFormat.ENCODING_PCM_8BIT */
    AUDIO_FORMAT_PCM_8_BIT           = (AUDIO_FORMAT_PCM |
                                        AUDIO_FORMAT_PCM_SUB_8_BIT),
    AUDIO_FORMAT_PCM_32_BIT          = (AUDIO_FORMAT_PCM |
                                        AUDIO_FORMAT_PCM_SUB_32_BIT),
    AUDIO_FORMAT_PCM_8_24_BIT        = (AUDIO_FORMAT_PCM |
                                        AUDIO_FORMAT_PCM_SUB_8_24_BIT),
    AUDIO_FORMAT_PCM_FLOAT           = (AUDIO_FORMAT_PCM |
                                        AUDIO_FORMAT_PCM_SUB_FLOAT),
    AUDIO_FORMAT_PCM_24_BIT_PACKED   = (AUDIO_FORMAT_PCM |
                                        AUDIO_FORMAT_PCM_SUB_24_BIT_PACKED),
    AUDIO_FORMAT_AAC_MAIN            = (AUDIO_FORMAT_AAC |
                                        AUDIO_FORMAT_AAC_SUB_MAIN),
    AUDIO_FORMAT_AAC_LC              = (AUDIO_FORMAT_AAC |
                                        AUDIO_FORMAT_AAC_SUB_LC),
    AUDIO_FORMAT_AAC_SSR             = (AUDIO_FORMAT_AAC |
                                        AUDIO_FORMAT_AAC_SUB_SSR),
    AUDIO_FORMAT_AAC_LTP             = (AUDIO_FORMAT_AAC |
                                        AUDIO_FORMAT_AAC_SUB_LTP),
    AUDIO_FORMAT_AAC_HE_V1           = (AUDIO_FORMAT_AAC |
                                        AUDIO_FORMAT_AAC_SUB_HE_V1),
    AUDIO_FORMAT_AAC_SCALABLE        = (AUDIO_FORMAT_AAC |
                                        AUDIO_FORMAT_AAC_SUB_SCALABLE),
    AUDIO_FORMAT_AAC_ERLC            = (AUDIO_FORMAT_AAC |
                                        AUDIO_FORMAT_AAC_SUB_ERLC),
    AUDIO_FORMAT_AAC_LD              = (AUDIO_FORMAT_AAC |
                                        AUDIO_FORMAT_AAC_SUB_LD),
    AUDIO_FORMAT_AAC_HE_V2           = (AUDIO_FORMAT_AAC |
                                        AUDIO_FORMAT_AAC_SUB_HE_V2),
    AUDIO_FORMAT_AAC_ELD             = (AUDIO_FORMAT_AAC |
                                        AUDIO_FORMAT_AAC_SUB_ELD),
    AUDIO_FORMAT_AAC_ADTS_MAIN       = (AUDIO_FORMAT_AAC_ADTS |
                                        AUDIO_FORMAT_AAC_SUB_MAIN),
    AUDIO_FORMAT_AAC_ADTS_LC         = (AUDIO_FORMAT_AAC_ADTS |
                                        AUDIO_FORMAT_AAC_SUB_LC),
    AUDIO_FORMAT_AAC_ADTS_SSR        = (AUDIO_FORMAT_AAC_ADTS |
                                        AUDIO_FORMAT_AAC_SUB_SSR),
    AUDIO_FORMAT_AAC_ADTS_LTP        = (AUDIO_FORMAT_AAC_ADTS |
                                        AUDIO_FORMAT_AAC_SUB_LTP),
    AUDIO_FORMAT_AAC_ADTS_HE_V1      = (AUDIO_FORMAT_AAC_ADTS |
                                        AUDIO_FORMAT_AAC_SUB_HE_V1),
    AUDIO_FORMAT_AAC_ADTS_SCALABLE   = (AUDIO_FORMAT_AAC_ADTS |
                                        AUDIO_FORMAT_AAC_SUB_SCALABLE),
    AUDIO_FORMAT_AAC_ADTS_ERLC       = (AUDIO_FORMAT_AAC_ADTS |
                                        AUDIO_FORMAT_AAC_SUB_ERLC),
    AUDIO_FORMAT_AAC_ADTS_LD         = (AUDIO_FORMAT_AAC_ADTS |
                                        AUDIO_FORMAT_AAC_SUB_LD),
    AUDIO_FORMAT_AAC_ADTS_HE_V2      = (AUDIO_FORMAT_AAC_ADTS |
                                        AUDIO_FORMAT_AAC_SUB_HE_V2),
    AUDIO_FORMAT_AAC_ADTS_ELD        = (AUDIO_FORMAT_AAC_ADTS |
                                        AUDIO_FORMAT_AAC_SUB_ELD),
    AUDIO_FORMAT_AAC_LATM_LC         = (AUDIO_FORMAT_AAC_LATM |\
                                        AUDIO_FORMAT_AAC_SUB_LC),
    AUDIO_FORMAT_AAC_LATM_HE_V1      = (AUDIO_FORMAT_AAC_LATM |\
                                        AUDIO_FORMAT_AAC_SUB_HE_V1),
    AUDIO_FORMAT_AAC_LATM_HE_V2      = (AUDIO_FORMAT_AAC_LATM |\
                                        AUDIO_FORMAT_AAC_SUB_HE_V2),
} audio_format_t;

typedef enum {
    /* These values must kept in sync with
     * frameworks/base/media/java/android/media/AudioSystem.java
     */
    AUDIO_STREAM_DEFAULT          = -1,
    AUDIO_STREAM_MIN              = 0,
    AUDIO_STREAM_VOICE_CALL       = 0,
    AUDIO_STREAM_SYSTEM           = 1,
    AUDIO_STREAM_RING             = 2,
    AUDIO_STREAM_MUSIC            = 3,
    AUDIO_STREAM_ALARM            = 4,
    AUDIO_STREAM_NOTIFICATION     = 5,
    AUDIO_STREAM_BLUETOOTH_SCO    = 6,
    AUDIO_STREAM_ENFORCED_AUDIBLE = 7, /* Sounds that cannot be muted by user
                                        * and must be routed to speaker
                                        */
    AUDIO_STREAM_DTMF             = 8,
    AUDIO_STREAM_TTS              = 9,  /* Transmitted Through Speaker.
                                         * Plays over speaker only, silent on other devices.
                                         */
    AUDIO_STREAM_ACCESSIBILITY    = 10, /* For accessibility talk back prompts */
    AUDIO_STREAM_REROUTING        = 11, /* For dynamic policy output mixes */
    AUDIO_STREAM_PATCH            = 12, /* For internal audio flinger tracks. Fixed volume */
    AUDIO_STREAM_PUBLIC_CNT       = AUDIO_STREAM_TTS + 1,
    AUDIO_STREAM_FOR_POLICY_CNT   = AUDIO_STREAM_PATCH, /* number of streams considered by
                                           audio policy for volume and routing */
    AUDIO_STREAM_CNT              = AUDIO_STREAM_PATCH + 1,
} audio_stream_type_t;

typedef enum {
    AUDIO_USAGE_UNKNOWN                            = 0,
    AUDIO_USAGE_MEDIA                              = 1,
    AUDIO_USAGE_VOICE_COMMUNICATION                = 2,
    AUDIO_USAGE_VOICE_COMMUNICATION_SIGNALLING     = 3,
    AUDIO_USAGE_ALARM                              = 4,
    AUDIO_USAGE_NOTIFICATION                       = 5,
    AUDIO_USAGE_NOTIFICATION_TELEPHONY_RINGTONE    = 6,
    AUDIO_USAGE_NOTIFICATION_COMMUNICATION_REQUEST = 7,
    AUDIO_USAGE_NOTIFICATION_COMMUNICATION_INSTANT = 8,
    AUDIO_USAGE_NOTIFICATION_COMMUNICATION_DELAYED = 9,
    AUDIO_USAGE_NOTIFICATION_EVENT                 = 10,
    AUDIO_USAGE_ASSISTANCE_ACCESSIBILITY           = 11,
    AUDIO_USAGE_ASSISTANCE_NAVIGATION_GUIDANCE     = 12,
    AUDIO_USAGE_ASSISTANCE_SONIFICATION            = 13,
    AUDIO_USAGE_GAME                               = 14,
    AUDIO_USAGE_VIRTUAL_SOURCE                     = 15,

    AUDIO_USAGE_CNT,
    AUDIO_USAGE_MAX                                = AUDIO_USAGE_CNT - 1,
} audio_usage_t;

typedef struct {
    uint16_t version;                   // version of the info structure
    uint16_t size;                      // total size of the structure including version and size
    uint32_t sample_rate;               // sample rate in Hz
    audio_channel_mask_t channel_mask;  // channel mask
    audio_format_t format;              // audio format
    audio_stream_type_t stream_type;    // stream type
    uint32_t bit_rate;                  // bit rate in bits per second
    int64_t duration_us;                // duration in microseconds, -1 if unknown
    bool has_video;                     // true if stream is tied to a video stream
    bool is_streaming;                  // true if streaming, false if local playback
    uint32_t bit_width;
    uint32_t offload_buffer_size;       // offload fragment size
    audio_usage_t usage;
} audio_offload_info_t;

struct audio_config {
    uint32_t sample_rate;
    audio_channel_mask_t channel_mask;
    audio_format_t  format;
    audio_offload_info_t offload_info;
    size_t frame_count;
};
typedef struct audio_config audio_config_t;

static inline size_t audio_bytes_per_sample(audio_format_t format)
{
    size_t size = 0;

    switch (format) {
    case AUDIO_FORMAT_PCM_32_BIT:
    case AUDIO_FORMAT_PCM_8_24_BIT:
        size = sizeof(int32_t);
        break;
    case AUDIO_FORMAT_PCM_24_BIT_PACKED:
        size = sizeof(uint8_t) * 3;
        break;
    case AUDIO_FORMAT_PCM_16_BIT:
    case AUDIO_FORMAT_IEC61937:
        size = sizeof(int16_t);
        break;
    case AUDIO_FORMAT_PCM_8_BIT:
        size = sizeof(uint8_t);
        break;
    case AUDIO_FORMAT_PCM_FLOAT:
        size = sizeof(float);
        break;
    default:
        break;
    }
    return size;
}

enum {

/* These can be a complete audio_channel_mask_t. */

    AUDIO_CHANNEL_NONE                      = 0x0,
    AUDIO_CHANNEL_INVALID                   = 0xC0000000,

/* These can be the bits portion of an audio_channel_mask_t
 * with representation AUDIO_CHANNEL_REPRESENTATION_POSITION.
 * Using these bits as a complete audio_channel_mask_t is deprecated.
 */

    /* output channels */
    AUDIO_CHANNEL_OUT_FRONT_LEFT            = 0x1,
    AUDIO_CHANNEL_OUT_FRONT_RIGHT           = 0x2,
    AUDIO_CHANNEL_OUT_FRONT_CENTER          = 0x4,
    AUDIO_CHANNEL_OUT_LOW_FREQUENCY         = 0x8,
    AUDIO_CHANNEL_OUT_BACK_LEFT             = 0x10,
    AUDIO_CHANNEL_OUT_BACK_RIGHT            = 0x20,
    AUDIO_CHANNEL_OUT_FRONT_LEFT_OF_CENTER  = 0x40,
    AUDIO_CHANNEL_OUT_FRONT_RIGHT_OF_CENTER = 0x80,
    AUDIO_CHANNEL_OUT_BACK_CENTER           = 0x100,
    AUDIO_CHANNEL_OUT_SIDE_LEFT             = 0x200,
    AUDIO_CHANNEL_OUT_SIDE_RIGHT            = 0x400,
    AUDIO_CHANNEL_OUT_TOP_CENTER            = 0x800,
    AUDIO_CHANNEL_OUT_TOP_FRONT_LEFT        = 0x1000,
    AUDIO_CHANNEL_OUT_TOP_FRONT_CENTER      = 0x2000,
    AUDIO_CHANNEL_OUT_TOP_FRONT_RIGHT       = 0x4000,
    AUDIO_CHANNEL_OUT_TOP_BACK_LEFT         = 0x8000,
    AUDIO_CHANNEL_OUT_TOP_BACK_CENTER       = 0x10000,
    AUDIO_CHANNEL_OUT_TOP_BACK_RIGHT        = 0x20000,
    AUDIO_CHANNEL_OUT_TOP_SIDE_LEFT         = 0x40000u,
    AUDIO_CHANNEL_OUT_TOP_SIDE_RIGHT        = 0x80000u,

/* TODO: should these be considered complete channel masks, or only bits? */

    AUDIO_CHANNEL_OUT_MONO     = AUDIO_CHANNEL_OUT_FRONT_LEFT,
    AUDIO_CHANNEL_OUT_STEREO   = (AUDIO_CHANNEL_OUT_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT),
    AUDIO_CHANNEL_OUT_2POINT1  = (AUDIO_CHANNEL_OUT_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT |
                                  AUDIO_CHANNEL_OUT_FRONT_CENTER),
    AUDIO_CHANNEL_OUT_QUAD     = (AUDIO_CHANNEL_OUT_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT |
                                  AUDIO_CHANNEL_OUT_BACK_LEFT |
                                  AUDIO_CHANNEL_OUT_BACK_RIGHT),
    AUDIO_CHANNEL_OUT_QUAD_BACK = AUDIO_CHANNEL_OUT_QUAD,
    /* like AUDIO_CHANNEL_OUT_QUAD_BACK with *_SIDE_* instead of *_BACK_* */
    AUDIO_CHANNEL_OUT_QUAD_SIDE = (AUDIO_CHANNEL_OUT_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT |
                                  AUDIO_CHANNEL_OUT_SIDE_LEFT |
                                  AUDIO_CHANNEL_OUT_SIDE_RIGHT),
    AUDIO_CHANNEL_OUT_SURROUND = (AUDIO_CHANNEL_OUT_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT |
                                  AUDIO_CHANNEL_OUT_FRONT_CENTER |
                                  AUDIO_CHANNEL_OUT_BACK_CENTER),
    AUDIO_CHANNEL_OUT_PENTA =    (AUDIO_CHANNEL_OUT_QUAD |
                                  AUDIO_CHANNEL_OUT_FRONT_CENTER),
    AUDIO_CHANNEL_OUT_5POINT1  = (AUDIO_CHANNEL_OUT_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT |
                                  AUDIO_CHANNEL_OUT_FRONT_CENTER |
                                  AUDIO_CHANNEL_OUT_LOW_FREQUENCY |
                                  AUDIO_CHANNEL_OUT_BACK_LEFT |
                                  AUDIO_CHANNEL_OUT_BACK_RIGHT),
    AUDIO_CHANNEL_OUT_5POINT1_BACK = AUDIO_CHANNEL_OUT_5POINT1,
    /* like AUDIO_CHANNEL_OUT_5POINT1_BACK with *_SIDE_* instead of *_BACK_* */
    AUDIO_CHANNEL_OUT_5POINT1_SIDE = (AUDIO_CHANNEL_OUT_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT |
                                  AUDIO_CHANNEL_OUT_FRONT_CENTER |
                                  AUDIO_CHANNEL_OUT_LOW_FREQUENCY |
                                  AUDIO_CHANNEL_OUT_SIDE_LEFT |
                                  AUDIO_CHANNEL_OUT_SIDE_RIGHT),
    AUDIO_CHANNEL_OUT_5POINT1POINT2 = (AUDIO_CHANNEL_OUT_5POINT1 |
                                       AUDIO_CHANNEL_OUT_TOP_SIDE_LEFT |
                                       AUDIO_CHANNEL_OUT_TOP_SIDE_RIGHT),
    AUDIO_CHANNEL_OUT_6POINT1  = (AUDIO_CHANNEL_OUT_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT |
                                  AUDIO_CHANNEL_OUT_FRONT_CENTER |
                                  AUDIO_CHANNEL_OUT_LOW_FREQUENCY |
                                  AUDIO_CHANNEL_OUT_BACK_LEFT |
                                  AUDIO_CHANNEL_OUT_BACK_RIGHT |
                                  AUDIO_CHANNEL_OUT_BACK_CENTER),
    // matches the correct AudioFormat.CHANNEL_OUT_7POINT1_SURROUND definition for 7.1
    AUDIO_CHANNEL_OUT_7POINT1  = (AUDIO_CHANNEL_OUT_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT |
                                  AUDIO_CHANNEL_OUT_FRONT_CENTER |
                                  AUDIO_CHANNEL_OUT_LOW_FREQUENCY |
                                  AUDIO_CHANNEL_OUT_BACK_LEFT |
                                  AUDIO_CHANNEL_OUT_BACK_RIGHT |
                                  AUDIO_CHANNEL_OUT_SIDE_LEFT |
                                  AUDIO_CHANNEL_OUT_SIDE_RIGHT),
    // matches the correct AudioFormat.CHANNEL_OUT_7POINT1POINT2 definition for 7.1.2
    AUDIO_CHANNEL_OUT_7POINT1POINT2  = (AUDIO_CHANNEL_OUT_7POINT1 |
                                  AUDIO_CHANNEL_OUT_FRONT_LEFT_OF_CENTER |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT_OF_CENTER),
    // matches the correct AudioFormat.CHANNEL_OUT_7POINT1_SURROUND definition for 7.1.4
    AUDIO_CHANNEL_OUT_7POINT1POINT4  = (AUDIO_CHANNEL_OUT_7POINT1 |
                                  AUDIO_CHANNEL_OUT_TOP_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_TOP_FRONT_RIGHT |
                                  AUDIO_CHANNEL_OUT_TOP_BACK_LEFT |
                                  AUDIO_CHANNEL_OUT_TOP_BACK_RIGHT),
    // matches the correct AudioFormat.CHANNEL_OUT_9POINT1_SURROUND definition for 9.1.6
    AUDIO_CHANNEL_OUT_9POINT1POINT6  = (AUDIO_CHANNEL_OUT_7POINT1POINT4 |
                                  AUDIO_CHANNEL_OUT_FRONT_LEFT_OF_CENTER |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT_OF_CENTER |
                                  AUDIO_CHANNEL_OUT_TOP_FRONT_CENTER |
                                  AUDIO_CHANNEL_OUT_TOP_BACK_CENTER),
    AUDIO_CHANNEL_OUT_ALL      = (AUDIO_CHANNEL_OUT_FRONT_LEFT |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT |
                                  AUDIO_CHANNEL_OUT_FRONT_CENTER |
                                  AUDIO_CHANNEL_OUT_LOW_FREQUENCY |
                                  AUDIO_CHANNEL_OUT_BACK_LEFT |
                                  AUDIO_CHANNEL_OUT_BACK_RIGHT |
                                  AUDIO_CHANNEL_OUT_FRONT_LEFT_OF_CENTER |
                                  AUDIO_CHANNEL_OUT_FRONT_RIGHT_OF_CENTER |
                                  AUDIO_CHANNEL_OUT_BACK_CENTER|
                                  AUDIO_CHANNEL_OUT_SIDE_LEFT|
                                  AUDIO_CHANNEL_OUT_SIDE_RIGHT|
                                  AUDIO_CHANNEL_OUT_TOP_CENTER|
                                  AUDIO_CHANNEL_OUT_TOP_FRONT_LEFT|
                                  AUDIO_CHANNEL_OUT_TOP_FRONT_CENTER|
                                  AUDIO_CHANNEL_OUT_TOP_FRONT_RIGHT|
                                  AUDIO_CHANNEL_OUT_TOP_BACK_LEFT|
                                  AUDIO_CHANNEL_OUT_TOP_BACK_CENTER|
                                  AUDIO_CHANNEL_OUT_TOP_BACK_RIGHT),

/* These are bits only, not complete values */

    /* input channels */
    AUDIO_CHANNEL_IN_LEFT            = 0x4,
    AUDIO_CHANNEL_IN_RIGHT           = 0x8,
    AUDIO_CHANNEL_IN_FRONT           = 0x10,
    AUDIO_CHANNEL_IN_BACK            = 0x20,
    AUDIO_CHANNEL_IN_LEFT_PROCESSED  = 0x40,
    AUDIO_CHANNEL_IN_RIGHT_PROCESSED = 0x80,
    AUDIO_CHANNEL_IN_FRONT_PROCESSED = 0x100,
    AUDIO_CHANNEL_IN_BACK_PROCESSED  = 0x200,
    AUDIO_CHANNEL_IN_PRESSURE        = 0x400,
    AUDIO_CHANNEL_IN_X_AXIS          = 0x800,
    AUDIO_CHANNEL_IN_Y_AXIS          = 0x1000,
    AUDIO_CHANNEL_IN_Z_AXIS          = 0x2000,
    AUDIO_CHANNEL_IN_VOICE_UPLINK    = 0x4000,
    AUDIO_CHANNEL_IN_VOICE_DNLINK    = 0x8000,

/* TODO: should these be considered complete channel masks, or only bits, or deprecated? */

    AUDIO_CHANNEL_IN_MONO   = AUDIO_CHANNEL_IN_FRONT,
    AUDIO_CHANNEL_IN_STEREO = (AUDIO_CHANNEL_IN_LEFT | AUDIO_CHANNEL_IN_RIGHT),
    AUDIO_CHANNEL_IN_FRONT_BACK = (AUDIO_CHANNEL_IN_FRONT | AUDIO_CHANNEL_IN_BACK),
    AUDIO_CHANNEL_IN_5POINT1 = (AUDIO_CHANNEL_IN_LEFT |
                               AUDIO_CHANNEL_IN_RIGHT |
                               AUDIO_CHANNEL_IN_FRONT |
                               AUDIO_CHANNEL_IN_BACK |
                               AUDIO_CHANNEL_IN_LEFT_PROCESSED |
                               AUDIO_CHANNEL_IN_RIGHT_PROCESSED),
    AUDIO_CHANNEL_IN_6 = 252u,
    AUDIO_CHANNEL_IN_VOICE_UPLINK_MONO = (AUDIO_CHANNEL_IN_VOICE_UPLINK | AUDIO_CHANNEL_IN_MONO),
    AUDIO_CHANNEL_IN_VOICE_DNLINK_MONO = (AUDIO_CHANNEL_IN_VOICE_DNLINK | AUDIO_CHANNEL_IN_MONO),
    AUDIO_CHANNEL_IN_VOICE_CALL_MONO   = (AUDIO_CHANNEL_IN_VOICE_UPLINK_MONO | AUDIO_CHANNEL_IN_VOICE_DNLINK_MONO),
    AUDIO_CHANNEL_IN_ALL    = (AUDIO_CHANNEL_IN_LEFT |
                               AUDIO_CHANNEL_IN_RIGHT |
                               AUDIO_CHANNEL_IN_FRONT |
                               AUDIO_CHANNEL_IN_BACK|
                               AUDIO_CHANNEL_IN_LEFT_PROCESSED |
                               AUDIO_CHANNEL_IN_RIGHT_PROCESSED |
                               AUDIO_CHANNEL_IN_FRONT_PROCESSED |
                               AUDIO_CHANNEL_IN_BACK_PROCESSED|
                               AUDIO_CHANNEL_IN_PRESSURE |
                               AUDIO_CHANNEL_IN_X_AXIS |
                               AUDIO_CHANNEL_IN_Y_AXIS |
                               AUDIO_CHANNEL_IN_Z_AXIS |
                               AUDIO_CHANNEL_IN_VOICE_UPLINK |
                               AUDIO_CHANNEL_IN_VOICE_DNLINK),
};

typedef enum {
    AUDIO_CHANNEL_REPRESENTATION_POSITION    = 0,    // must be zero for compatibility
    // 1 is reserved for future use
    AUDIO_CHANNEL_REPRESENTATION_INDEX       = 2,
    // 3 is reserved for future use
} audio_channel_representation_t;

static inline audio_channel_representation_t audio_channel_mask_get_representation(
        audio_channel_mask_t channel)
{
    // The right shift should be sufficient, but also "and" for safety in case mask is not 32 bits
    return (audio_channel_representation_t)
            ((channel >> AUDIO_CHANNEL_COUNT_MAX) & ((1 << AUDIO_CHANNEL_REPRESENTATION_LOG2) - 1));
}

static inline uint32_t audio_channel_mask_get_bits(audio_channel_mask_t channel)
{
    return channel & ((1 << AUDIO_CHANNEL_COUNT_MAX) - 1);
}

static inline uint32_t audio_channel_count_from_in_mask(audio_channel_mask_t channel)
{
    uint32_t bits = audio_channel_mask_get_bits(channel);
    switch (audio_channel_mask_get_representation(channel)) {
    case AUDIO_CHANNEL_REPRESENTATION_POSITION:
        // TODO: We can now merge with from_out_mask and remove anding
        bits &= AUDIO_CHANNEL_IN_ALL;
        // fall through
    case AUDIO_CHANNEL_REPRESENTATION_INDEX:
        return popcount(bits);
    default:
        return 0;
    }
}

#else
#include <system/audio.h>
#endif
#define SOUND_TRIGGER_MAX_STRING_LEN 64 /* max length of strings in properties or
                                           descriptor structs */
#define SOUND_TRIGGER_MAX_LOCALE_LEN 6  /* max length of locale string. e.g en_US */
#define SOUND_TRIGGER_MAX_USERS 10      /* max number of concurrent users */
#define SOUND_TRIGGER_MAX_PHRASES 10    /* max number of concurrent phrases */

typedef enum {
    SOUND_TRIGGER_STATE_NO_INIT = -1,   /* The sound trigger service is not initialized */
    SOUND_TRIGGER_STATE_ENABLED = 0,    /* The sound trigger service is enabled */
    SOUND_TRIGGER_STATE_DISABLED = 1    /* The sound trigger service is disabled */
} sound_trigger_service_state_t;

#define RECOGNITION_MODE_VOICE_TRIGGER 0x1       /* simple voice trigger */
#define RECOGNITION_MODE_USER_IDENTIFICATION 0x2 /* trigger only if one user in model identified */
#define RECOGNITION_MODE_USER_AUTHENTICATION 0x4 /* trigger only if one user in mode
                                                    authenticated */
#define RECOGNITION_MODE_GENERIC_TRIGGER 0x8
#define RECOGNITION_STATUS_SUCCESS 0
#define RECOGNITION_STATUS_ABORT 1
#define RECOGNITION_STATUS_FAILURE 2

#define SOUND_MODEL_STATUS_UPDATED 0

typedef enum {
    SOUND_MODEL_TYPE_UNKNOWN = -1,    /* use for unspecified sound model type */
    SOUND_MODEL_TYPE_KEYPHRASE = 0,    /* use for key phrase sound models */
    SOUND_MODEL_TYPE_GENERIC = 1       /* use for all models other than keyphrase */
} sound_trigger_sound_model_type_t;

typedef struct sound_trigger_uuid_s {
    unsigned int   timeLow;
    unsigned short timeMid;
    unsigned short timeHiAndVersion;
    unsigned short clockSeq;
    unsigned char  node[6];
} sound_trigger_uuid_t;

/*
 * sound trigger implementation descriptor read by the framework via get_properties().
 * Used by SoundTrigger service to report to applications and manage concurrency and policy.
 */
struct sound_trigger_properties {
    char                 implementor[SOUND_TRIGGER_MAX_STRING_LEN]; /* implementor name */
    char                 description[SOUND_TRIGGER_MAX_STRING_LEN]; /* implementation description */
    unsigned int         version;               /* implementation version */
    sound_trigger_uuid_t uuid;                  /* unique implementation ID.
                                                   Must change with version each version */
    unsigned int         max_sound_models;      /* maximum number of concurrent sound models
                                                   loaded */
    unsigned int         max_key_phrases;       /* maximum number of key phrases */
    unsigned int         max_users;             /* maximum number of concurrent users detected */
    unsigned int         recognition_modes;     /* all supported modes.
                                                   e.g RECOGNITION_MODE_VOICE_TRIGGER */
    bool                 capture_transition;    /* supports seamless transition from detection
                                                   to capture */
    unsigned int         max_buffer_ms;         /* maximum buffering capacity in ms if
                                                   capture_transition is true*/
    bool                 concurrent_capture;    /* supports capture by other use cases while
                                                   detection is active */
    bool                 trigger_in_event;      /* returns the trigger capture in event */
    unsigned int         power_consumption_mw;  /* Rated power consumption when detection is active
                                                   with TDB silence/sound/speech ratio */
};

typedef int sound_trigger_module_handle_t;

struct sound_trigger_module_descriptor {
    sound_trigger_module_handle_t   handle;
    struct sound_trigger_properties properties;
};

typedef int sound_model_handle_t;

/*
 * Generic sound model descriptor. This struct is the header of a larger block passed to
 * load_sound_model() and containing the binary data of the sound model.
 * Proprietary representation of users in binary data must match information indicated
 * by users field
 */
struct sound_trigger_sound_model {
    sound_trigger_sound_model_type_t type;        /* model type. e.g. SOUND_MODEL_TYPE_KEYPHRASE */
    sound_trigger_uuid_t             uuid;        /* unique sound model ID. */
    sound_trigger_uuid_t             vendor_uuid; /* unique vendor ID. Identifies the engine the
                                                  sound model was build for */
    unsigned int                     data_size;   /* size of opaque model data */
    unsigned int                     data_offset; /* offset of opaque data start from head of struct
                                                    (e.g sizeof struct sound_trigger_sound_model) */
};

/* key phrase descriptor */
struct sound_trigger_phrase {
    unsigned int id;                /* keyphrase ID */
    unsigned int recognition_mode;  /* recognition modes supported by this key phrase */
    unsigned int num_users;         /* number of users in the key phrase */
    unsigned int users[SOUND_TRIGGER_MAX_USERS]; /* users ids: (not uid_t but sound trigger
                                                 specific IDs */
    char         locale[SOUND_TRIGGER_MAX_LOCALE_LEN]; /* locale - JAVA Locale style (e.g. en_US) */
    char         text[SOUND_TRIGGER_MAX_STRING_LEN];   /* phrase text in UTF-8 format. */
};

/*
 * Specialized sound model for key phrase detection.
 * Proprietary representation of key phrases in binary data must match information indicated
 * by phrases field
 */
struct sound_trigger_phrase_sound_model {
    struct sound_trigger_sound_model common;
    unsigned int                     num_phrases;   /* number of key phrases in model */
    struct sound_trigger_phrase      phrases[SOUND_TRIGGER_MAX_PHRASES];
};


/*
 * Generic recognition event sent via recognition callback
 */
struct sound_trigger_recognition_event {
    int                              status;            /* recognition status e.g.
                                                           RECOGNITION_STATUS_SUCCESS */
    sound_trigger_sound_model_type_t type;              /* event type, same as sound model type.
                                                           e.g. SOUND_MODEL_TYPE_KEYPHRASE */
    sound_model_handle_t             model;             /* loaded sound model that triggered the
                                                           event */
    bool                             capture_available; /* it is possible to capture audio from this
                                                           utterance buffered by the
                                                           implementation */
    int                              capture_session;   /* audio session ID. framework use */
    int                              capture_delay_ms;  /* delay in ms between end of model
                                                           detection and start of audio available
                                                           for capture. A negative value is possible
                                                           (e.g. if key phrase is also available for
                                                           capture */
    int                              capture_preamble_ms; /* duration in ms of audio captured
                                                            before the start of the trigger.
                                                            0 if none. */
    bool                             trigger_in_data; /* the opaque data is the capture of
                                                            the trigger sound */
    audio_config_t                   audio_config;        /* audio format of either the trigger in
                                                             event data or to use for capture of the
                                                             rest of the utterance */
    uint64_t                         timestamp;       /* time stamp at the time of detection */

    unsigned int                     data_size;         /* size of opaque event data */
    unsigned int                     data_offset;       /* offset of opaque data start from start of
                                                          this struct (e.g sizeof struct
                                                          sound_trigger_phrase_recognition_event) */
};

/*
 * Confidence level for each user in struct sound_trigger_phrase_recognition_extra
 */
struct sound_trigger_confidence_level {
    unsigned int user_id;   /* user ID */
    unsigned int level;     /* confidence level in percent (0 - 100).
                               - min level for recognition configuration
                               - detected level for recognition event */
};

/*
 * Specialized recognition event for key phrase detection
 */
struct sound_trigger_phrase_recognition_extra {
    unsigned int id;                /* keyphrase ID */
    unsigned int recognition_modes; /* recognition modes used for this keyphrase */
    unsigned int confidence_level;  /* confidence level for mode RECOGNITION_MODE_VOICE_TRIGGER */
    unsigned int num_levels;        /* number of user confidence levels */
    struct sound_trigger_confidence_level levels[SOUND_TRIGGER_MAX_USERS];
};

struct sound_trigger_phrase_recognition_event {
    struct sound_trigger_recognition_event common;
    unsigned int                           num_phrases;
    struct sound_trigger_phrase_recognition_extra phrase_extras[SOUND_TRIGGER_MAX_PHRASES];
};

struct sound_trigger_generic_recognition_event {
    struct sound_trigger_recognition_event common;
};
/*
 * configuration for sound trigger capture session passed to start_recognition()
 */
struct sound_trigger_recognition_config {
    audio_io_handle_t    capture_handle;    /* IO handle that will be used for capture.
                                            N/A if capture_requested is false */
    audio_devices_t      capture_device;    /* input device requested for detection capture */
    bool                 capture_requested; /* capture and buffer audio for this recognition
                                            instance */
    unsigned int         num_phrases;   /* number of key phrases recognition extras */
    struct sound_trigger_phrase_recognition_extra phrases[SOUND_TRIGGER_MAX_PHRASES];
                                           /* configuration for each key phrase */
    unsigned int        data_size;         /* size of opaque capture configuration data */
    unsigned int        data_offset;       /* offset of opaque data start from start of this struct
                                           (e.g sizeof struct sound_trigger_recognition_config) */
};

/*
 * Event sent via load sound model callback
 */
struct sound_trigger_model_event {
    int                  status;      /* sound model status e.g. SOUND_MODEL_STATUS_UPDATED */
    sound_model_handle_t model;       /* loaded sound model that triggered the event */
    unsigned int         data_size;   /* size of event data if any. Size of updated sound model if
                                       status is SOUND_MODEL_STATUS_UPDATED */
    unsigned int         data_offset; /* offset of data start from start of this struct
                                       (e.g sizeof struct sound_trigger_model_event) */
};


#endif  // ANDROID_SOUND_TRIGGER_H
