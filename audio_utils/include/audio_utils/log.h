/*
 * Copyright (C) 2005-2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <syslog.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef ALOGD
#define ALOGD(fmt, arg...) syslog (LOG_DEBUG, LOG_TAG fmt, ##arg)
#endif

#ifndef ALOGI
#define ALOGI(fmt, arg...) syslog (LOG_INFO, LOG_TAG fmt, ##arg)
#endif

#ifndef ALOGV
#define ALOGV(fmt, arg...) syslog (LOG_NOTICE, LOG_TAG fmt, ##arg)
#endif

#ifndef ALOGE
#define ALOGE(fmt, arg...) syslog (LOG_ERR, LOG_TAG fmt, ##arg)
#endif

#ifndef ALOGW
#define ALOGW(fmt, arg...) syslog (LOG_WARNING, LOG_TAG fmt, ##arg)
#endif

#ifndef LOG_BUF_SIZE
#define LOG_BUF_SIZE 1024
#endif

static int inline __android_log_write(int prio, const char *tag, const char *msg)
{
     syslog(prio, "%s - %s", tag, msg);
     return 0;
}

static void inline __android_log_assert(const char *cond, const char *tag,
                           const char *fmt, ...)
{
     char buf[LOG_BUF_SIZE];

     if (fmt) {
         va_list ap;
         va_start(ap, fmt);
         vsnprintf(buf, LOG_BUF_SIZE, fmt, ap);
         va_end(ap);
     } else {
         /* Msg not provided, log condition.  N.B. Do not use cond directly as
          * format string as it could contain spurious '%' syntax (e.g.
          * "%d" in "blocks%devs == 0").
          */
         if (cond)
             snprintf(buf, LOG_BUF_SIZE, "Assertion failed: %s", cond);
         else
             strlcpy(buf, "Unspecified assertion failed", LOG_BUF_SIZE);
     }

     __android_log_write(LOG_CRIT, tag, buf);
     abort(); /* abort so we have a chance to debug the situation */
     /* NOTREACHED */
}

#ifndef __android_rest
#define __android_rest(first, ...)               , ## __VA_ARGS__
#endif

#ifndef __android_second
#define __android_second(dummy, second, ...)     second
#endif

#ifndef android_printAssert
#define android_printAssert(cond, tag, fmt...) \
     __android_log_assert(cond, tag, \
         __android_second(0, ## fmt, NULL) __android_rest(fmt))
#endif

#ifndef LOG_ALWAYS_FATAL
#define LOG_ALWAYS_FATAL(...) \
     ( ((void)android_printAssert(NULL, LOG_TAG, ## __VA_ARGS__)) )
#endif
